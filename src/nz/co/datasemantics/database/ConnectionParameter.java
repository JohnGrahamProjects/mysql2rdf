package nz.co.datasemantics.database;

public enum ConnectionParameter {
    HOST,
    PORT,
    TABLE_SCHEMA,
    USERNAME,
    PASSWORD
}
