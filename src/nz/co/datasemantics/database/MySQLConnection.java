package nz.co.datasemantics.database;

import javax.swing.*;
import java.sql.*;
import java.util.*;

public class MySQLConnection {
    private final Map<ConnectionParameter, String> connectionParams;
    private Connection connection;
    private String tableSchema;

    public MySQLConnection(Map<ConnectionParameter, String> connectionParams) {
        this.tableSchema = connectionParams.get(ConnectionParameter.TABLE_SCHEMA);
        this.connectionParams = connectionParams;
    }

    public boolean connect(JTextArea jTextArea) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + connectionParams.get(ConnectionParameter.HOST) + ":" + connectionParams.get(ConnectionParameter.PORT) + "/"
                            + this.tableSchema,
                    connectionParams.get(ConnectionParameter.USERNAME),
                    connectionParams.get(ConnectionParameter.PASSWORD));
            return true;
        } catch (Exception e) {
            jTextArea.append(e.getMessage());
            System.out.println("Exception thrown in MySQLConnection.connect.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
        }
        return false;
    }

    public List<String> getTableNames() {
        ArrayList<String> tableNames = new ArrayList<>();
        String query = "select table_name from information_schema.tables" +
                " where information_schema.tables.table_schema='" + tableSchema + "'";
        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next())
                tableNames.add(rs.getString(1));
        } catch (SQLException e) {
            System.out.println("Exception thrown in MySQLConnection.getTableNames Method.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
            System.out.println("QUERY: " + query);
        }
        return tableNames;
    }

    public List<EnumMap<Metadata, Object>> getPrimaryKeys(String tableName) {
        ArrayList<EnumMap<Metadata, Object>> primaryKeyMetadata = new ArrayList<>();
        String query = "SELECT table_name, column_name, data_type FROM information_schema.columns " +
                "WHERE table_name = '" + tableName + "' " +
                "AND information_schema.columns.table_schema = '" + tableSchema + "' " +
                "AND column_key = 'PRI'";
        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                EnumMap<Metadata, Object> primaryKey = new EnumMap<Metadata, Object>(Metadata.class);
                primaryKeyMetadata.add(primaryKey);
                String columnName;
                primaryKey.put(Metadata.COLUMN_NAME, columnName = rs.getString(Metadata.COLUMN_NAME.toString()));
                primaryKey.put(Metadata.TABLE_NAME, rs.getString(Metadata.TABLE_NAME.toString()));
                primaryKey.put(Metadata.DATA_TYPE, this.getDataType(tableName, columnName));
            }
        } catch (SQLException e) {
            System.out.println("Exception thrown in MySQLConnection.getPrimaryKeys Method.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
            System.out.println("QUERY: " + query);
        }
        return primaryKeyMetadata;
    }

    public List<EnumMap<Metadata, Object>> getForeignKeyNames(String tableName) {
        ArrayList<EnumMap<Metadata, Object>> foreignKeyMetadata = new ArrayList<>();
        String query = "SELECT * FROM " +
                "INFORMATION_SCHEMA.KEY_COLUMN_USAGE " +
                "WHERE REFERENCED_TABLE_SCHEMA = '" + tableSchema + "'" +
                "AND TABLE_NAME = '" + tableName + "'" +
                "and key_column_usage.referenced_table_name is not null";
        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                EnumMap<Metadata, Object> foreignKey = new EnumMap<Metadata, Object>(Metadata.class);
                foreignKeyMetadata.add(foreignKey);
                String columnName;
                foreignKey.put(Metadata.COLUMN_NAME, columnName = rs.getString(Metadata.COLUMN_NAME.toString()));
                foreignKey.put(Metadata.TABLE_NAME, rs.getString(Metadata.TABLE_NAME.toString()));
                foreignKey.put(Metadata.REFERENCED_TABLE_NAME, rs.getString(Metadata.REFERENCED_TABLE_NAME.toString()));
                foreignKey.put(Metadata.REFERENCED_COLUMN_NAME, rs.getString(Metadata.REFERENCED_COLUMN_NAME.toString()));
                foreignKey.put(Metadata.DATA_TYPE, this.getDataType(tableName, columnName));
            }
        } catch (SQLException e) {
            System.out.println("Exception thrown in MySQLConnection.getForeignKeyNames Method.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
            System.out.println("QUERY: " + query);
        }
        return foreignKeyMetadata;
    }

    public List<EnumMap<Metadata, Object>> getNonKeyNames(String tableName) {
        ArrayList<EnumMap<Metadata, Object>> nonKeyMetadata = new ArrayList<>();
        String query = "select table_name, column_name, data_type from (select table_name, column_name, data_type from information_schema.columns where information_schema.columns.table_schema='" + tableSchema + "' and information_schema.columns.table_name='" + tableName + "') as T1\n" +
                "where column_name not in (select column_name from (select information_schema.columns.column_name from information_schema.columns where information_schema.columns.table_schema='" + tableSchema + "' and information_schema.columns.column_key='pri' and information_schema.columns.table_name='" + tableName + "') as T2 \n" +
                "union (select information_schema.key_column_usage.column_name from information_schema.key_column_usage where information_schema.key_column_usage.table_schema='" + tableSchema + "' and information_schema.key_column_usage.referenced_table_name is not null and information_schema.key_column_usage.table_name='" + tableName + "'))";
        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(query)) {

            while (rs.next()) {
                EnumMap<Metadata, Object> foreignKey = new EnumMap<Metadata, Object>(Metadata.class);
                nonKeyMetadata.add(foreignKey);
                String columnName;
                foreignKey.put(Metadata.COLUMN_NAME, columnName = rs.getString(Metadata.COLUMN_NAME.toString()));
                foreignKey.put(Metadata.TABLE_NAME, rs.getString(Metadata.TABLE_NAME.toString()));
                foreignKey.put(Metadata.DATA_TYPE, this.getDataType(tableName, columnName));
            }
        } catch (SQLException e) {
            System.out.println("Exception thrown in MySQLConnection.getNonKeyNames Method.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
            System.out.println("QUERY: " + query);
        }
        return nonKeyMetadata;
    }

    public List<HashMap<String, Object>> getRowData(String tableName) {
        ResultSet result = null;
        try (Statement stm = connection.createStatement()) {
            boolean returningRows = stm.execute("select * from `" + tableSchema + "`.`" + tableName + "`");
            if (returningRows)
                result = stm.getResultSet();
            else
                return new ArrayList<>();

            //get metadata
            ResultSetMetaData meta = null;
            meta = result.getMetaData();

            //get column names
            int colCount = meta.getColumnCount();
            ArrayList<String> cols = new ArrayList<String>();
            for (int index = 1; index <= colCount; index++)
                cols.add(meta.getColumnName(index));

            //fetch out rows
            ArrayList<HashMap<String, Object>> rows =
                    new ArrayList<HashMap<String, Object>>();

            while (result.next()) {
                HashMap<String, Object> row = new HashMap<String, Object>();
                for (String colName : cols) {
                    Object val = result.getObject(colName);
                    row.put(colName, val);
                }
                rows.add(row);
            }

            //close statement
            stm.close();
            result.close();

            //pass back rows
            return rows;
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            return new ArrayList<>();
        }
    }

    public String getDataType(String tableName, String columnName) {
        String query = "SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS " +
                "  WHERE table_name = '" + tableName + "' AND COLUMN_NAME = '" + columnName + "';";

        String dataType = null;
        try (Statement st = connection.createStatement(); ResultSet rs = st.executeQuery(query)) {
            while (rs.next()) {
                dataType = rs.getString("DATA_TYPE");
            }
            st.close();
        } catch (SQLException e) {
            System.out.println("Exception thrown in MySQLConnection.getDataType Method.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
            System.out.println("QUERY: " + query);
        }
        return dataType;
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("Exception thrown in MySQLConnection.closeConnection Method.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
        }
    }
}

