package nz.co.datasemantics.database;


public enum Metadata {

    TABLE_NAME("table_name"),
    COLUMN_NAME("column_name"),
    REFERENCED_TABLE_NAME("referenced_table_name"),
    REFERENCED_COLUMN_NAME("referenced_column_name"),
    DATA_TYPE("data_type");
    private String string;

    Metadata(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return string;
    }
}
