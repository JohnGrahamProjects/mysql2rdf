package nz.co.datasemantics.model.owl;

class Class extends NamedObject implements CanHaveEquivalence {
    private String equivalentProperty = "";
    private Namespace equivalentPropertyNamepace = new Namespace("", "");

    Class(Namespace namespace, String name) {
        super(namespace, name);
    }

    public String getEquivalence() {
        return equivalentProperty;
    }

    public void setEquivalence(String equivalentProperty) {
        this.equivalentProperty = equivalentProperty;
    }

    public Namespace getEquivalenceNamespace() {
        return equivalentPropertyNamepace;
    }

    void setEquivalenceNamespace(Namespace namespace) {
        this.equivalentPropertyNamepace = namespace;
    }

    public void setEquivalenceNamespace(String fullyQualified, String prefix) {
        this.equivalentPropertyNamepace.set(fullyQualified, prefix);
    }
}
