package nz.co.datasemantics.model.owl;

import java.util.ArrayList;
import java.util.List;

public class DataTypeProperty extends NamedObject implements Property, CanHaveEquivalence {
    private String equivalentProperty = "";
    private Namespace equivalentPropertyNamepace = new Namespace("", "");
    private ArrayList<Class> domains;
    private ArrayList<Class> ranges;


    DataTypeProperty(Namespace namespace, String name) {

        super(namespace, name);
        domains = new ArrayList<>();
        ranges = new ArrayList<>();
    }

    public String getEquivalence() {
        return equivalentProperty;
    }

    public void setEquivalence(String equivalentProperty) {
        this.equivalentProperty = equivalentProperty;
    }

    public Namespace getEquivalenceNamespace() {
        return equivalentPropertyNamepace;
    }

    void setEquivalenceNamespace(Namespace namespace) {
        this.equivalentPropertyNamepace = namespace;
    }

    public void setEquivalenceNamespace(String fullyQualified, String prefix) {
        this.equivalentPropertyNamepace.set(fullyQualified, prefix);
    }

    public void addDomain(Class domain) {
        domains.add(domain);
    }

    public List<Class> getDomains() {
        return domains;
    }

    public void addRange(Class range) {
        ranges.add(range);
    }

    public List<Class> getRanges() {
        return ranges;
    }
}