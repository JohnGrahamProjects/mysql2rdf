package nz.co.datasemantics.model.owl;

import java.io.OutputStream;
import java.util.List;

public class Serialiser {
    public Serialiser() {

    }

    public String serialise_ttl(OWL owl, OutputStream outputStream) {
        String finalString = "";

        List<Namespace> namespaces;

        if (!(namespaces = owl.getNamespaces()).isEmpty()) {
            finalString += "# NAMESPACES\n\n";
        }

        for (Namespace namespace : namespaces) {
            finalString += "@prefix ";

            finalString += namespace.getPrefix() + ": ";
            finalString += "<" + namespace.getFullyQualified() + "> .\n";
        }

        finalString += "\n# ONTOLOGY\n\n";
        finalString += owl.getNamespace().getPrefix() + ":" + owl.getName() + " rdf:type owl:ontology .\n";

        List<Class> classes;
        if (!(classes = owl.getClasses()).isEmpty()) {
            finalString += "\n# CLASSES\n\n";
        }

        for (Class clazz : classes) {
            if (!clazz.getEquivalence().equals("")) {
                finalString += clazz.getNamespace().getPrefix() + ":";
                finalString += clazz.getName() + " ";
                finalString += "owl:equivalentClass ";
                finalString += clazz.getEquivalenceNamespace().getPrefix() + ":";
                finalString += clazz.getEquivalence() + ".\n";
            }
//else {
            finalString += clazz.getNamespace().getPrefix() + ":";
            finalString += clazz.getName() + " ";
            finalString += "rdf:type ";
            finalString += "owl:Class .\n";
//        }
        }

        List<ObjectProperty> objectProperties;
        if (!(objectProperties = owl.getObjectProperties()).isEmpty()) {
            finalString += "\n# OBJECT PROPERTIES\n\n";
        }

        for (ObjectProperty objectProperty : objectProperties) {
            if (!objectProperty.getEquivalence().equals("")) {
                finalString += objectProperty.getNamespace().getPrefix() + ":";
                finalString += objectProperty.getName() + " ";
                finalString += "owl:equivalentProperty ";
                finalString += objectProperty.getEquivalenceNamespace().getPrefix() + ":";
                finalString += objectProperty.getEquivalence() + ".\n";
            }
//else {
            finalString += objectProperty.getNamespace().getPrefix() + ":";
            finalString += objectProperty.getName() + " ";
            finalString += "rdf:type ";
            finalString += "owl:ObjectProperty";
            finalString += " ;\n";
            finalString += "\trdfs:subPropertyOf owl:topObjectProperty";
//        }
            List<Class> ranges = objectProperty.getRanges();

            String rangeString = "";
            int rangeCount = ranges.size();
            if (rangeCount == 0) {

            } else if (rangeCount == 1) {
                finalString += " ;\n";

                Class range = ranges.get(0);
                rangeString += range.getNamespace().getPrefix() + ":" + range.getName();
            } else if (rangeCount > 1) {
                for (Class range : ranges) {
                    finalString += " ;\n";
                    rangeString += "\n";
                    rangeString += "\t\t" + range.getNamespace().getPrefix() + ":" + range.getName();
                }
            }

            switch (rangeCount) {
                case 0:
                    break;
                case 1:
                    finalString += "\trdfs:range ";
                    finalString += rangeString + " ";
                    break;
                default:
                    finalString += "\n\trdfs:range ";
                    finalString += "[ ";
                    finalString += "rdf:type ";
                    finalString += "owl:Class ";
                    finalString += ";\n";
                    finalString += "\t\towl:unionOf ";
                    finalString += "(\n";
                    finalString += rangeString + " ";
                    finalString += ";\n";
                    finalString += "\t\t) ";
                    finalString += "\t] ";
            }

            List<Class> domains = objectProperty.getDomains();

            String domainString = "";
            int domainCount = domains.size();
            if (domainCount == 0) {
                finalString += ".\n";
            } else if (domainCount == 1) {
                finalString += ";\n";

                Class domain = domains.get(0);
                domainString += " " + domain.getNamespace().getPrefix() + ":";
                domainString += domain.getName();
            } else if (domainCount > 1) {
                finalString += ";\n";
                for (Class domain : domains) {
                    rangeString += "\n";

                    domainString += "\t\t\t" + domain.getNamespace().getPrefix() + ":";
                    domainString += domain.getName();
                    domainString += "\n";
                }
            }

            switch (domainCount) {
                case 0:
                    break;
                case 1:
                    finalString += "\trdfs:domain ";
                    finalString += domainString + " ";
                    break;
                default:
                    finalString += "\trdfs:domain ";
                    finalString += "[ ";
                    finalString += "rdf:type ";
                    finalString += "owl:Class ";
                    finalString += ";\n";
                    finalString += "\t\towl:unionOf ";
                    finalString += "(\n";
                    finalString += domainString + " ";
                    finalString += "\t\t) ";
                    finalString += "\n\t] ";
            }
            finalString += ".\n";
        }
        List<DataTypeProperty> dataTypeProperties;
        if (!(dataTypeProperties = owl.getDataTypeProperties()).isEmpty()) {
            finalString += "\n# DATATYPE PROPERTIES\n";
        }

        for (DataTypeProperty dataTypeProperty : dataTypeProperties) {
            finalString += "\n";

            finalString += dataTypeProperty.getNamespace().getPrefix() + ":";
            finalString += dataTypeProperty.getName() + " ";
            finalString += "rdf:type ";
            finalString += "owl:DatatypeProperty";
            finalString += " ;\n";
            finalString += "\trdfs:subPropertyOf owl:topDataProperty";

            List<Class> ranges = dataTypeProperty.getRanges();

            String rangeString = "";
            int rangeCount = ranges.size();
            if (rangeCount == 0) {

            } else if (rangeCount == 1) {
                finalString += " ;\n";

                Class range = ranges.get(0);
                rangeString += " " + range.getNamespace().getPrefix() + ":";
                rangeString += range.getName();
            } else if (rangeCount > 1) {
                for (Class range : ranges) {
                    finalString += " ;\n";
                    rangeString += "\n";
                    rangeString += "\t\t" + range.getNamespace().getPrefix() + ":";
                    rangeString += range.getName();
                }
            }

            switch (rangeCount) {
                case 0:
                    break;
                case 1:
                    finalString += "\trdfs:range ";
                    finalString += rangeString + " ";
                    break;
                default:
                    finalString += "\n\trdfs:range ";
                    finalString += "[ ";
                    finalString += "rdf:type ";
                    finalString += "owl:Class ";
                    finalString += ";\n";
                    finalString += "\t\towl:unionOf ";
                    finalString += "(\n";
                    finalString += rangeString + " ";
                    finalString += ";\n";
                    finalString += "\t\t) ";
                    finalString += "\t] ";
            }

            List<Class> domains = dataTypeProperty.getDomains();

            String domainString = "";
            int domainCount = domains.size();
            if (domainCount == 0) {
                finalString += ".\n";
            } else if (domainCount == 1) {
                finalString += ";\n";

                Class domain = domains.get(0);
                domainString += " " + domain.getNamespace().getPrefix() + ":";
                domainString += domain.getName();
            } else if (domainCount > 1) {
                for (Class domain : domains) {
                    finalString += ";\n";
                    rangeString += "\n";
                    domainString += "\t\t\t" + domain.getNamespace().getPrefix() + ":";
                    domainString += domain.getName();
                    domainString += "\n";
                }
            }

            switch (domainCount) {
                case 0:
                    break;
                case 1:
                    finalString += "\trdfs:domain ";
                    finalString += domainString + " ";
                    break;
                default:
                    finalString += "\trdfs:domain ";
                    finalString += "[ ";
                    finalString += "rdf:type ";
                    finalString += "owl:Class ";
                    finalString += ";\n";
                    finalString += "\t\towl:unionOf ";
                    finalString += "(\n";
                    finalString += domainString + " ";
                    finalString += "\t\t) ";
                    finalString += "\n\t] ";
            }
            finalString += ".";
        }
        return finalString;
    }
}
