package nz.co.datasemantics.model.owl;

public interface CanHaveEquivalence {
    String getEquivalence();

    void setEquivalence(String equivalence);

    nz.co.datasemantics.model.owl.Namespace getEquivalenceNamespace();

    void setEquivalenceNamespace(String fullyQualified, String prefix);
}
