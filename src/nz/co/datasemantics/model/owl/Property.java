package nz.co.datasemantics.model.owl;

import java.util.List;

public interface Property {
    void addDomain(Class domain);

    List<Class> getDomains();

    void addRange(Class range);

    List<Class> getRanges();
}
