package nz.co.datasemantics.model.owl;

public class Namespace {

    private String fullyQualified;
    private String prefix;

    public Namespace(String fullyQualified, String prefix) {
        this.set(fullyQualified, prefix);
    }

    public void set(String fullyQualified, String prefix) {
        if (!fullyQualified.equals("") && !fullyQualified.startsWith("http://"))
            fullyQualified = "http://" + fullyQualified;
        if (!fullyQualified.equals("") && fullyQualified.charAt(fullyQualified.length() - 1) == '/')
            fullyQualified = fullyQualified.substring(0, fullyQualified.length() - 1);
        if (!fullyQualified.equals("") && fullyQualified.charAt(fullyQualified.length() - 1) != '#')
            this.fullyQualified = fullyQualified + '#';
        else
            this.fullyQualified = fullyQualified;
        this.prefix = prefix;
    }

    public String getFullyQualified() {
        return fullyQualified;
    }

    public String getPrefix() {
        return prefix;
    }

    @Override
    public boolean equals(Object namespace) {

        return namespace != null
                && namespace instanceof Namespace
                && fullyQualified.equals(((Namespace) namespace).getFullyQualified());
    }
}
