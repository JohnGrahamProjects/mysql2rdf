package nz.co.datasemantics.model.owl;

import nz.co.datasemantics.model.relational.ForeignKey;
import nz.co.datasemantics.model.relational.NonKey;
import nz.co.datasemantics.model.relational.Table;
import nz.co.datasemantics.model.relational.TableSchema;

import java.util.ArrayList;
import java.util.List;

public class OWL extends NamedObject {

    private static final Namespace NAMESPACE = new Namespace("http://www.w3.org/2002/07/owl#", "owl");
    private ArrayList<Namespace> namespaces;
    private ArrayList<Class> classes;
    private ArrayList<ObjectProperty> objectProperties;
    private ArrayList<DataTypeProperty> dataTypeProperties;

    public OWL(Namespace namespace, String name) {
        super(namespace, name);
        classes = new ArrayList<>();
        objectProperties = new ArrayList<>();
        dataTypeProperties = new ArrayList<>();
        namespaces = new ArrayList<>();
        namespaces.add(namespace);
        namespaces.add(OWL.NAMESPACE);
        namespaces.add(RDFS.NAMESPACE);
        namespaces.add(XML.NAMESPACE);
        namespaces.add(nz.co.datasemantics.model.owl.RDF.NAMESPACE);
    }

    List<Namespace> getNamespaces() {
        return namespaces;
    }

    List<Class> getClasses() {
        return classes;
    }

    List<ObjectProperty> getObjectProperties() {
        return objectProperties;
    }

    List<DataTypeProperty> getDataTypeProperties() {
        return dataTypeProperties;
    }

    private void addClass(Class clazz) {
        classes.add(clazz);
        addNamespace(clazz.getNamespace());
    }

    private void addObjectProperty(ObjectProperty objectProperty) {
        objectProperties.add(objectProperty);
        addNamespace(objectProperty.getNamespace());
    }

    private void addDataTypeProperty(DataTypeProperty datatypeProperty) {
        dataTypeProperties.add(datatypeProperty);
        addNamespace(datatypeProperty.getNamespace());
    }

    private void addNamespace(Namespace newNamespace) {
        for (Namespace namespace : namespaces) {
            if (namespace.equals(newNamespace)) {
                return;
            }
        }
        namespaces.add(newNamespace);
    }

    public void buildGraph(TableSchema tableSchema) {
        List<Table> tables = tableSchema.getTables();

        for (Table table : tables) {
            String className = table.getTerm();
            nz.co.datasemantics.model.owl.Class clazz = new nz.co.datasemantics.model.owl.Class(table.getNamespace(), className);
            addClass(clazz);

            if (!table.getEquivalence().equals("")) {
                addNamespace(table.getEquivalenceNamespace());
                clazz.setEquivalence(table.getEquivalence());
                clazz.setEquivalenceNamespace(table.getEquivalenceNamespace());
            }

            List<ForeignKey> foreignKeys = table.getForeignKeys();

            for (ForeignKey foreignKey : foreignKeys) {
                String propertyName = foreignKey.getTerm();
                ObjectProperty objectProperty = addObjectProperty(foreignKey.getNamespace(), propertyName);
                objectProperty.addDomain(clazz);
                objectProperty.addRange(new Class(tableSchema.getTable(foreignKey.getReferencedTableName()).getNamespace(), foreignKey.getReferencedTableName().substring(0, 1).toUpperCase() + foreignKey.getReferencedTableName().substring(1)));

                if (!foreignKey.getEquivalence().equals("")) {
                    addNamespace(foreignKey.getEquivalenceNamespace());
                    objectProperty.setEquivalence(foreignKey.getEquivalence());
                    objectProperty.setEquivalenceNamespace(foreignKey.getEquivalenceNamespace());
                }
            }

            List<NonKey> nonKeys = table.getNonKeys();

            for (NonKey nonKey : nonKeys) {
                String propertyName = nonKey.getTerm();
                DataTypeProperty datatypeProperty = new DataTypeProperty(nonKey.getNamespace(), propertyName);
                addDataTypeProperty(datatypeProperty);
                datatypeProperty.addDomain(clazz);
                Literal xsdLiteral = new Literal(nonKey.getDataType().toString());
                addNamespace(xsdLiteral.getNamespace());
                datatypeProperty.addRange(xsdLiteral);

                if (!nonKey.getEquivalence().equals("")) {
                    addNamespace(nonKey.getEquivalenceNamespace());
                    datatypeProperty.setEquivalence(nonKey.getEquivalence());
                    datatypeProperty.setEquivalenceNamespace(nonKey.getEquivalenceNamespace());
                }
            }
        }
    }

    private ObjectProperty addObjectProperty(Namespace namespace, String name) {
        for (ObjectProperty existingObjectProperty : objectProperties) {
            if (existingObjectProperty.getNamespace().equals(namespace) && existingObjectProperty.getName().equals(name)) {
                return existingObjectProperty;
            }
        }
        ObjectProperty objectProperty = new ObjectProperty(namespace, name);
        addObjectProperty(objectProperty);
        return objectProperty;
    }
}