package nz.co.datasemantics.model.owl;

public class DataType {

    private static final DataType CHARACTER = new DataType("string");
    private static final DataType VARCHAR = new DataType("string");
    private static final DataType DOUBLE = new DataType("decimal");
    private static final DataType INTEGER = new DataType("integer");
    private static final DataType DECIMAL = new DataType("decimal");
    private static final DataType BOOLEAN = new DataType("boolean");
    private static final DataType DATE = new DataType("date");
    private static final DataType TIME = new DataType("time");
    public final nz.co.datasemantics.model.owl.Namespace NAMESPACE = nz.co.datasemantics.model.owl.XSD.NAMESPACE;
    private final String XSD;

    private DataType(String XSD) {
        this.XSD = XSD;
    }

    public static DataType autoDetermine(String MySQLDataType) {
        if (MySQLDataType.contains("var"))
            return VARCHAR;
        if (MySQLDataType.contains("char"))
            return CHARACTER;
        if (MySQLDataType.contains("doub"))
            return DOUBLE;
        if (MySQLDataType.contains("int"))
            return INTEGER;
        if (MySQLDataType.contains("decim"))
            return DECIMAL;
        if (MySQLDataType.contains("bool"))
            return BOOLEAN;
        if (MySQLDataType.equals("date"))
            return DATE;
        if (MySQLDataType.equals("time"))
            return TIME;
        else return VARCHAR;
    }

    public String toString() {
        return XSD;
    }
}
