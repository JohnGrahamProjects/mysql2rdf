package nz.co.datasemantics.model.owl;


public abstract class NamedObject {
    private Namespace namespace;
    private String name;

    NamedObject(Namespace namespace, String name) {
        this.namespace = namespace;
        this.name = name;
    }

    public Namespace getNamespace() {
        return namespace;
    }

    public String getName() {
        return name;
    }
}
