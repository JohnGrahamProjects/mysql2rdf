package nz.co.datasemantics.model.owl;

import java.util.ArrayList;
import java.util.List;

public class ObjectProperty extends NamedObject implements Property, CanHaveEquivalence {
    private String equivalentProperty = "";
    private Namespace equivalentPropertyNamepace = new Namespace("", "");
    private ArrayList<Class> domains;
    private ArrayList<Class> ranges;


    ObjectProperty(Namespace namespace, String name) {
        super(namespace, name);
        domains = new ArrayList<>();
        ranges = new ArrayList<>();
    }

    public String getEquivalence() {
        return equivalentProperty;
    }

    public void setEquivalence(String equivalentProperty) {
        this.equivalentProperty = equivalentProperty;
    }

    public Namespace getEquivalenceNamespace() {
        return equivalentPropertyNamepace;
    }

    public void setEquivalenceNamespace(Namespace namespace) {
        this.equivalentPropertyNamepace = namespace;
    }

    public void setEquivalenceNamespace(String fullyQualified, String prefix) {
        this.equivalentPropertyNamepace.set(fullyQualified, prefix);
    }

    public void addDomain(Class newDomain) {
        for (Class existingDomain : domains) {
            if (existingDomain.getNamespace().equals(newDomain.getNamespace()) && existingDomain.getName().equals(newDomain.getName()))
                return;
        }
        domains.add(newDomain);
    }

    public List<Class> getDomains() {
        return domains;
    }

    public void addRange(Class range) {
        for (Class clazz : ranges) {
            if (clazz.getNamespace().equals(range.getNamespace()) && clazz.getName().equals(range.getName()))
                return;
        }
        ranges.add(range);
    }

    public List<Class> getRanges() {
        return ranges;
    }
}
