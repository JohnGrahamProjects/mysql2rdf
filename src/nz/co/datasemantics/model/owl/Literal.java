package nz.co.datasemantics.model.owl;


class Literal extends Class {
    Literal(String name) {
        super(XSD.NAMESPACE, name);
    }
}
