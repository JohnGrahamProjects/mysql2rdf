package nz.co.datasemantics.model.rdf;

public class Predicate extends Resource {
    private nz.co.datasemantics.model.rdf.Object object;

    Predicate(nz.co.datasemantics.model.owl.Namespace namespace, String hashIdentifier) {
        super(namespace, hashIdentifier, null);
    }

    public Object getObject() {
        return object;
    }

    public void setObject(nz.co.datasemantics.model.rdf.Object object) {
        this.object = object;
    }
}