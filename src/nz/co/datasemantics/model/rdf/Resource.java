package nz.co.datasemantics.model.rdf;

public class Resource {
    private nz.co.datasemantics.model.owl.Namespace namespace;
    private String hashIdentifier;
    private String name;

    Resource(nz.co.datasemantics.model.owl.Namespace namespace, String hashIdentifier, String name) {
        this.namespace = namespace;
        this.hashIdentifier = hashIdentifier;
        if(name != null)
            this.name = name.replaceAll("\"","");
        else
            this.name = name;
    }

    public nz.co.datasemantics.model.owl.Namespace getNamespace() {
        return namespace;
    }

    String getHashIdentifier() {
        return hashIdentifier;
    }

    public String getName() {
        return name;
    }
}
