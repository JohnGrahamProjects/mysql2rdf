package nz.co.datasemantics.model.rdf;

import nz.co.datasemantics.model.owl.DataType;

class Literal extends Object {
    private DataType datatype;

    Literal(nz.co.datasemantics.model.owl.Namespace namespace, String value, DataType datatype) {
        super(namespace, null, value);
        this.datatype = datatype;
    }

    DataType getDataType() {
        return this.datatype;
    }
}