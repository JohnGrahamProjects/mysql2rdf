package nz.co.datasemantics.model.rdf;

class Object extends Resource {
    Object(nz.co.datasemantics.model.owl.Namespace namespace, String hashIdentifier, String name) {
        super(namespace, hashIdentifier, name);
    }
}