package nz.co.datasemantics.model.rdf;

import nz.co.datasemantics.model.owl.Namespace;

import java.util.ArrayList;
import java.util.List;

class Subject extends Object {

    private ArrayList<Predicate> predicates;

    Subject(Namespace namespace, String hashIdentifier, String name) {
        super(namespace, hashIdentifier, name);
        predicates = new ArrayList<>();
    }

    void addPredicate(Predicate predicate) {
        predicates.add(predicate);
    }

    List<Predicate> getPredicates() {
        return predicates;
    }
}
