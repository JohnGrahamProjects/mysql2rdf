package nz.co.datasemantics.model.rdf;

public class Namespace {
    private String fullyQualified;
    private String prefix;

    public Namespace(String fullyQualified, String prefix) {
        if (fullyQualified.charAt(fullyQualified.length() - 1) != '/')
            this.fullyQualified = fullyQualified + '/';
        else
            this.fullyQualified = fullyQualified;

        this.prefix = prefix;
    }
}
