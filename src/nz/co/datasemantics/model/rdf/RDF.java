package nz.co.datasemantics.model.rdf;

import nz.co.datasemantics.database.MySQLConnection;
import nz.co.datasemantics.model.relational.*;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.Object;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RDF extends Resource {
    public static final nz.co.datasemantics.model.owl.Namespace NAMESPACE = new nz.co.datasemantics.model.owl.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf");
    private List<nz.co.datasemantics.model.owl.Namespace> namespaces;
    private List<Subject> subjects;

    public RDF(nz.co.datasemantics.model.owl.Namespace namespace) {
        super(namespace, "", "");

        namespaces = new ArrayList<>();
        namespaces.add(RDF.NAMESPACE);

    }

    private void addNamespace(nz.co.datasemantics.model.owl.Namespace newNamespace) {
        for (nz.co.datasemantics.model.owl.Namespace namespace : namespaces) {
            if (namespace.equals(newNamespace)) {
                return;
            }
        }
        namespaces.add(newNamespace);
    }

    public void buildAndSerialiseGraph(MySQLConnection mySQLConnection, TableSchema tableSchema, String tableName, OutputStream outputStream) {
        nz.co.datasemantics.model.rdf.Serialiser rdfSerialiser = new nz.co.datasemantics.model.rdf.Serialiser();
        namespaces.add(tableSchema.getNamespace());

        List<Table> tables = tableSchema.getTables();
        for (Table table : tables) {
            if (tableName != null && !tableName.toLowerCase().equals(table.getName().toLowerCase())) continue;

            addNamespace(table.getNamespace());
            for (PrimaryKey primaryKey : table.getPrimaryKeys()) {
                addNamespace(primaryKey.getNamespace());
            }
            for (ForeignKey foreignKey : table.getForeignKeys()) {
                addNamespace(foreignKey.getNamespace());
            }
            for (NonKey nonKey : table.getNonKeys()) {
                addNamespace(nonKey.getNamespace());
                addNamespace(nonKey.getDataType().NAMESPACE);
            }
        }
        try {
            rdfSerialiser.serialiseNamespaces(namespaces, outputStream);
        } catch (IOException e) {
            System.out.println("Exception thrown in RDF.buildAndSerialiseGraph Method.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
        }

        for (Table table : tables) {
            if (tableName != null && !tableName.toLowerCase().equals(table.getName().toLowerCase())) continue;
            // clear subjects for this new table
            subjects = new ArrayList<>();
            nz.co.datasemantics.model.rdf.Object object = new nz.co.datasemantics.model.rdf.Object(table.getNamespace(), table.getTerm(), "");
            List<HashMap<String, Object>> rows = mySQLConnection.getRowData(table.getName());
            Subject subject;
            for (HashMap<String, Object> row : rows) {
                List<PrimaryKey> primaryKeys = table.getPrimaryKeys();
                if (!primaryKeys.isEmpty()) {
                    StringBuilder value = new StringBuilder();
                    for (PrimaryKey primaryKey : primaryKeys) {
                        value.append(value.toString().length() == 0 ? "" : ";");
                        if (primaryKey.getName() == null)
                            throw new IllegalArgumentException("NO VALUE IN PRIMARY KEY.");
                        value.append(primaryKey.getName()).append("=").append(row.get(primaryKey.getName()).toString());
                    }

                    subject = new Subject(table.getNamespace(), table.getTerm(), value.toString());
                    Predicate predicate = new Predicate(RDF.NAMESPACE, "type");
                    subject.addPredicate(predicate);
                    predicate.setObject(object);

                } else {
                    subject = new BlankNode();
                    Predicate predicate = new Predicate(RDF.NAMESPACE, "type");
                    subject.addPredicate(predicate);
                    predicate.setObject(object);
                }

                subjects.add(subject);
                List<ForeignKey> foreignKeys = table.getForeignKeys();
                for (ForeignKey foreignKey : foreignKeys) {
                    Predicate predicate = new Predicate(foreignKey.getNamespace(), foreignKey.getTerm());

                    subject.addPredicate(predicate);
                    String foreignKeyValue = foreignKey.getReferencedColumnName() + "=" + (row.get(foreignKey.getName()) == null ? "" : row.get(foreignKey.getName()).toString());
                    nz.co.datasemantics.model.rdf.Object obj = new nz.co.datasemantics.model.rdf.Object(tableSchema.getTable(foreignKey.getReferencedTableName()).getNamespace(), foreignKey.getReferencedTableName(), foreignKeyValue);
                    predicate.setObject(obj);
                }

                List<NonKey> nonKeys = table.getNonKeys();
                for (NonKey nonKey : nonKeys) {
                    Predicate predicate = new Predicate(nonKey.getNamespace(), nonKey.getTerm());
                    subject.addPredicate(predicate);
                    String value = (row.get(nonKey.getName()) == null ? "" : row.get(nonKey.getName()).toString());
                    nz.co.datasemantics.model.rdf.Object obj = new nz.co.datasemantics.model.rdf.Literal(nonKey.getNamespace(), value, nonKey.getDataType());

                    predicate.setObject(obj);
                }
            }
            try {
                rdfSerialiser.serialiseTriples(subjects, outputStream);
            } catch (IOException e) {
                System.out.println("Exception thrown in RDF.buildAndSerialiseGraph Method.");
                System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
            }
        }
    }
}
