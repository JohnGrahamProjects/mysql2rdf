package nz.co.datasemantics.model.rdf;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;

public class Serialiser {

    public void serialiseTriples(List<Subject> subjects, OutputStream outputStream) throws IOException {
        if (!subjects.isEmpty()) {
            outputStream.write(("# TRIPLES\n\n").getBytes(Charset.forName("UTF-8")));
        }
        for (Subject subject : subjects) {
            if (subject.getNamespace() == null) {
                outputStream.write(("[").getBytes(Charset.forName("UTF-8")));
            } else {
                outputStream.write(("<" + subject.getNamespace().getPrefix() + ":" + subject.getHashIdentifier().substring(0, 1).toUpperCase() + subject.getHashIdentifier().substring(1) + "/" + subject.getName() + "> ").getBytes(Charset.forName("UTF-8")));
            }

            List<Predicate> predicates = subject.getPredicates();
            int i = predicates.size();
            for (Predicate predicate : subject.getPredicates()) {
                outputStream.write(("\n\t").getBytes(Charset.forName("UTF-8")));
                outputStream.write((predicate.getNamespace().getPrefix() + ":" + predicate.getHashIdentifier() + " ").getBytes(Charset.forName("UTF-8")));

                nz.co.datasemantics.model.rdf.Object object = predicate.getObject();

                if (object.getHashIdentifier() == null) {
                    outputStream.write(("\"" + object.getName() + "\"^^<" + ((Literal) object).getDataType().NAMESPACE.getPrefix() + ":" + ((Literal) object).getDataType().toString() + ">").getBytes(Charset.forName("UTF-8")));
                } else {
                    outputStream.write(("<" + object.getNamespace().getPrefix() + ":" + object.getHashIdentifier().substring(0, 1).toUpperCase() + object.getHashIdentifier().substring(1) + (object.getName().equals("") ? "" : "/") + object.getName() + "> ").getBytes(Charset.forName("UTF-8")));
                }
                i--;
                if (i > 0) {
                    outputStream.write((" ;").getBytes(Charset.forName("UTF-8")));
                }
            }
            if (subject.getNamespace() == null) {
                outputStream.write(("\n] .\n").getBytes(Charset.forName("UTF-8")));
            } else {
                outputStream.write((" .\n").getBytes(Charset.forName("UTF-8")));
            }
            outputStream.write(("\n").getBytes(Charset.forName("UTF-8")));
        }
    }

    void serialiseNamespaces(List<nz.co.datasemantics.model.owl.Namespace> namespaces, OutputStream outputStream) throws IOException {

        if (!namespaces.isEmpty()) {
            outputStream.write(("# NAMESPACES\n\n").getBytes(Charset.forName("UTF-8")));
        }

        for (nz.co.datasemantics.model.owl.Namespace namespace : namespaces) {
            outputStream.write(("@prefix ").getBytes(Charset.forName("UTF-8")));
            outputStream.write((namespace.getPrefix() + ": ").getBytes(Charset.forName("UTF-8")));
            outputStream.write(("<" + namespace.getFullyQualified() + "> .\n").getBytes(Charset.forName("UTF-8")));
        }
        outputStream.write(("\n").getBytes(Charset.forName("UTF-8")));
    }
}
