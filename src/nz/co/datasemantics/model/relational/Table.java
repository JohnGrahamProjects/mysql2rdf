package nz.co.datasemantics.model.relational;

import nz.co.datasemantics.database.Metadata;
import nz.co.datasemantics.database.MySQLConnection;
import nz.co.datasemantics.model.owl.CanHaveEquivalence;
import nz.co.datasemantics.model.owl.Namespace;
import nz.co.datasemantics.model.treetable.node.TableNode;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

public class Table extends TableNode implements CanHaveEquivalence {

    private final JTextField fullNamespace = new JTextField();
    private final JTextField namespacePrefix = new JTextField();
    private final JTextField className = new JTextField();
    private String equivalentClass = "";
    private Namespace equivalentClassNamepace = new Namespace("", "");
    private ArrayList<PrimaryKey> primaryKeys;
    private ArrayList<ForeignKey> foreignKeys;
    private ArrayList<NonKey> nonKeys;
    Table(String name) {

        super(new String[]{name, name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase(), "", "", "", ""});
        primaryKeys = new ArrayList<>();
        foreignKeys = new ArrayList<>();
        nonKeys = new ArrayList<>();
    }

    public String getEquivalence() {
        return equivalentClass;
    }

    public void setEquivalence(String equivalentClass) {
        this.equivalentClass = equivalentClass;
    }

    public Namespace getEquivalenceNamespace() {
        return equivalentClassNamepace;
    }

    public void setEquivalenceNamespace(String fullyQualified, String prefix) {
        this.equivalentClassNamepace.set(fullyQualified, prefix);
        for (ForeignKey foreignKey : getForeignKeys()) {
            foreignKey.setEquivalenceNamespace(fullyQualified, prefix);
        }
        for (NonKey nonKey : getNonKeys()) {
            nonKey.setEquivalenceNamespace(fullyQualified, prefix);
        }
    }

    private void setClassName(String className) {
        String classNameTemp = className.substring(0, 1).toUpperCase() + className.substring(1).toLowerCase();
        this.className.setText(classNameTemp);
        setValueAt(classNameTemp, 1);
    }

    private void addColumn(Column column) {
        super.add(column);
        if (column instanceof PrimaryKey)
            primaryKeys.add((PrimaryKey) column);
        else if (column instanceof ForeignKey)
            foreignKeys.add((ForeignKey) column);
        else
            nonKeys.add((NonKey) column);
    }

    public List<PrimaryKey> getPrimaryKeys() {
        return primaryKeys;
    }

    public List<ForeignKey> getForeignKeys() {
        return foreignKeys;
    }

    public List<NonKey> getNonKeys() {
        return nonKeys;
    }

    void buildSchema(MySQLConnection mySQLConnection) {

        List<EnumMap<Metadata, Object>> primaryKeysTemp = mySQLConnection.getPrimaryKeys(super.getName());
        for (EnumMap<Metadata, Object> primaryKeyMetadata : primaryKeysTemp) {
            Column column = new PrimaryKey(primaryKeyMetadata);
            addColumn(column);
        }

        List<EnumMap<Metadata, Object>> foreignKeysTemp = mySQLConnection.getForeignKeyNames(super.getName());
        for (EnumMap<Metadata, Object> foreignKey : foreignKeysTemp) {
            Column column = new ForeignKey(foreignKey);
            addColumn(column);
        }

        List<EnumMap<Metadata, Object>> nonKeysTemp = mySQLConnection.getNonKeyNames(super.getName());
        for (EnumMap<Metadata, Object> nonKey : nonKeysTemp) {
            Column column = new NonKey(nonKey);
            addColumn(column);
        }
    }

    @Override
    public String iconPathString() {

        if (getEquivalence().equals(""))
            return "/table_24x24.png";
        return "/table_24x24_ticked.png";
    }

    @Override
    public TableCellEditor getCellEditor(int columnIndex) {
        if (columnIndex == 0 || columnIndex > 3) return null;
        final JTextField jtextField =
                columnIndex == 1 ? className :
                        columnIndex == 2 ? fullNamespace : namespacePrefix;

        DefaultCellEditor cellEditor = new DefaultCellEditor(jtextField);
        cellEditor.addCellEditorListener(new CellEditorListener() {

            @Override
            public void editingStopped(ChangeEvent e) {

                if (columnIndex == 2)
                    setFullNamespace(jtextField.getText());
                else if (columnIndex == 3)
                    setNamespacePrefix(jtextField.getText());
                else if (columnIndex == 1)
                    setClassName(jtextField.getText());
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
            }

        });
        return cellEditor;
    }

    void setFullNamespace(String fullNamespace) {
        this.fullNamespace.setText(fullNamespace);
        setValueAt(fullNamespace, 2);
        for (PrimaryKey primaryKey : primaryKeys) {
            primaryKey.setFullNamespace(fullNamespace);
        }
        for (ForeignKey foreignKey : foreignKeys) {
            foreignKey.setFullNamespace(fullNamespace);
        }
        for (NonKey nonKey : nonKeys) {
            nonKey.setFullNamespace(fullNamespace);
        }
    }

    void setNamespacePrefix(String namespacePrefix) {
        this.namespacePrefix.setText(namespacePrefix);
        setValueAt(namespacePrefix, 3);
        for (PrimaryKey primaryKey : primaryKeys) {
            primaryKey.setNamespacePrefix(namespacePrefix);
        }
        for (ForeignKey foreignKey : foreignKeys) {
            foreignKey.setNamespacePrefix(namespacePrefix);
        }
        for (NonKey nonKey : nonKeys) {
            nonKey.setNamespacePrefix(namespacePrefix);
        }
    }

    public Namespace getNamespace() {
        return new Namespace(this.fullNamespace.getText(), this.namespacePrefix.getText());
    }
}