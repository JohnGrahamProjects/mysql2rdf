package nz.co.datasemantics.model.relational;

import nz.co.datasemantics.database.Metadata;

import java.util.EnumMap;

public class PrimaryKey extends Column {
    PrimaryKey(EnumMap<Metadata, Object> primaryKeyMetadata) {
        super(new String[]{
                primaryKeyMetadata.get(Metadata.COLUMN_NAME).toString(),
                primaryKeyMetadata.get(Metadata.COLUMN_NAME).toString(),
                "",
                "",
                "",
                ""});
    }

    public String iconPathString() {
        return "/primary_24x24.png";
    }

    @Override
    public boolean isEditable(int columnIndex) {
        return columnIndex < 4;
    }
}