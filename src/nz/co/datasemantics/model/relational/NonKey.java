package nz.co.datasemantics.model.relational;

import nz.co.datasemantics.database.Metadata;
import nz.co.datasemantics.model.owl.CanHaveEquivalence;
import nz.co.datasemantics.model.owl.DataType;
import nz.co.datasemantics.model.owl.Namespace;

import java.util.EnumMap;

public class NonKey extends Column implements CanHaveEquivalence {

    private String equivalentProperty = "";
    private Namespace equivalentPropertyNamepace = new Namespace("", "");
    private DataType dataType;

    NonKey(EnumMap<Metadata, Object> nonKeyMetadata) {
        super(new String[]{
                nonKeyMetadata.get(Metadata.COLUMN_NAME).toString(),
                nonKeyMetadata.get(Metadata.COLUMN_NAME).toString(),
                "",
                "",
                nonKeyMetadata.get(Metadata.TABLE_NAME).toString().substring(0, 1).toUpperCase() + nonKeyMetadata.get(Metadata.TABLE_NAME).toString().substring(1).toLowerCase(),
                DataType.autoDetermine(nonKeyMetadata.get(Metadata.DATA_TYPE).toString()).toString()});
        dataType = DataType.autoDetermine(nonKeyMetadata.get(Metadata.DATA_TYPE).toString());
    }

    public String getEquivalence() {
        return equivalentProperty;
    }

    public void setEquivalence(String equivalentProperty) {
        this.equivalentProperty = equivalentProperty;
    }

    public Namespace getEquivalenceNamespace() {
        return equivalentPropertyNamepace;
    }

    public void setEquivalenceNamespace(String fullyQualified, String prefix) {
        this.equivalentPropertyNamepace.set(fullyQualified, prefix);
    }

    public String iconPathString() {
        if (getEquivalence().equals(""))
            return "/non_24x24.png";
        return "/non_24x24_ticked.png";
    }

    public DataType getDataType() {
        return dataType;
    }

    @Override
    public boolean isEditable(int columnIndex) {
        return columnIndex > 0 && columnIndex < 4;
    }
}
