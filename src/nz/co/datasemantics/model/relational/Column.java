package nz.co.datasemantics.model.relational;

import nz.co.datasemantics.model.owl.Namespace;
import nz.co.datasemantics.model.treetable.node.ColumnNode;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

public abstract class Column extends ColumnNode {

    final JTextField range = new JTextField();
    private final JTextField propertyName = new JTextField();
    private final JTextField fullNamespace = new JTextField();
    private final JTextField namespacePrefix = new JTextField();
    private final JTextField domain = new JTextField();

    Column(String[] stringObject) {
        super(stringObject);
    }

    public Namespace getNamespace() {
        return new Namespace(this.fullNamespace.getText(), this.namespacePrefix.getText());
    }

    private void setPropertyName(String propertyName) {
        String propertyNameTemp = propertyName.toLowerCase();
        this.propertyName.setText(propertyNameTemp);
        setValueAt(propertyNameTemp, 1);
    }

    private void setDomainName(String domainName) {
        String domainNameTemp = domainName.substring(0, 1).toUpperCase() + domainName.substring(1).toLowerCase();
        this.domain.setText(domainNameTemp);
        setValueAt(domainNameTemp, 4);
    }

    protected void setRangeName(String rangeName) {
        String rangeNameTemp = rangeName.toLowerCase();
        this.range.setText(rangeNameTemp);
        setValueAt(rangeNameTemp, 5);
    }

    @Override
    public TableCellEditor getCellEditor(int columnIndex) {
        if (columnIndex == 0 || columnIndex > 5) return null;
        final JTextField jtextField =
                columnIndex == 1 ? propertyName :
                        columnIndex == 2 ? fullNamespace :
                                columnIndex == 3 ? namespacePrefix :
                                        columnIndex == 4 ? domain : range;

        DefaultCellEditor cellEditor = new DefaultCellEditor(jtextField);
        cellEditor.addCellEditorListener(new CellEditorListener() {

            @Override
            public void editingStopped(ChangeEvent e) {

                if (columnIndex == 1)
                    setPropertyName(jtextField.getText());
                else if (columnIndex == 2)
                    setFullNamespace(jtextField.getText());
                else if (columnIndex == 3)
                    setNamespacePrefix(jtextField.getText());
                else if (columnIndex == 4)
                    setDomainName(jtextField.getText());
                else if (columnIndex == 5)
                    setRangeName(jtextField.getText());
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
            }

        });
        return cellEditor;
    }

    void setFullNamespace(String fullNamespace) {
        this.fullNamespace.setText(fullNamespace);
        setValueAt(fullNamespace, 2);
    }

    void setNamespacePrefix(String namespacePrefix) {
        this.namespacePrefix.setText(namespacePrefix);
        setValueAt(namespacePrefix, 3);
    }
}