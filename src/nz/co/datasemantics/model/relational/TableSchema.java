package nz.co.datasemantics.model.relational;

import nz.co.datasemantics.database.MySQLConnection;
import nz.co.datasemantics.model.treetable.node.TableSchemaNode;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import java.util.ArrayList;
import java.util.List;

public class TableSchema extends TableSchemaNode {

    private final JTextField fullNamespace = new JTextField();
    private final JTextField namespacePrefix = new JTextField();
    private final JTextField ontologyName = new JTextField();

    private ArrayList<Table> tables;

    public TableSchema(String name) {
        super(new String[]{name, name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase(), "", "", "", ""});
        tables = new ArrayList<>();
    }

    public String getFullNamespace() {
        return this.fullNamespace.getText();
    }

    public void setFullNamespace(String fullNamespace) {
        this.fullNamespace.setText(fullNamespace);
        setValueAt(fullNamespace, 2);
        for (Table table : tables) {
            table.setFullNamespace(fullNamespace);
        }
    }

    public String getNamespacePrefix() {
        return this.namespacePrefix.getText();
    }

    public void setNamespacePrefix(String namespacePrefix) {
        this.namespacePrefix.setText(namespacePrefix);
        setValueAt(namespacePrefix, 3);
        for (Table table : tables) {
            table.setNamespacePrefix(namespacePrefix);
        }
    }

    private void setOntologyName(String ontologyName) {
        String ontologyNameTemp = ontologyName.substring(0, 1).toUpperCase() + ontologyName.substring(1).toLowerCase();
        this.ontologyName.setText(ontologyNameTemp);
        setValueAt(ontologyNameTemp, 1);
    }

    private void addTable(Table table) {
        super.add(table);
        tables.add(table);
    }

    public List<Table> getTables() {
        return tables;
    }

    public void buildSchema(MySQLConnection mySQLConnection) {
        List<String> tableNames = mySQLConnection.getTableNames();
        for (String tableName : tableNames) {
            Table table = new Table(tableName);
            addTable(table);
            table.buildSchema(mySQLConnection);
        }
    }

    @Override
    public String iconPathString() {
        return "/database_24x24.png";
    }

    @Override
    public TableCellEditor getCellEditor(int columnIndex) {
        if (columnIndex == 0 || columnIndex > 3) return null;
        final JTextField jtextField =
                columnIndex == 1 ? ontologyName :
                        columnIndex == 2 ? fullNamespace : namespacePrefix;

        DefaultCellEditor cellEditor = new DefaultCellEditor(jtextField);

        cellEditor.addCellEditorListener(new CellEditorListener() {

            @Override
            public void editingStopped(ChangeEvent e) {
                if (columnIndex == 2)
                    setFullNamespace(jtextField.getText());
                else if (columnIndex == 3)
                    setNamespacePrefix(jtextField.getText());
                else if (columnIndex == 1)
                    setOntologyName(jtextField.getText());
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
            }

        });
        return cellEditor;
    }

    public Table getTable(String s) {
        for (Table table : tables) {
            if (table.getValueAt(0).toString().equals(s))
                return table;
        }
        return null;
    }

    public nz.co.datasemantics.model.owl.Namespace getNamespace() {
        return new nz.co.datasemantics.model.owl.Namespace(this.fullNamespace.getText(), this.namespacePrefix.getText());
    }
}
