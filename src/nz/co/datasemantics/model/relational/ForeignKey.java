package nz.co.datasemantics.model.relational;

import nz.co.datasemantics.database.Metadata;
import nz.co.datasemantics.model.owl.CanHaveEquivalence;
import nz.co.datasemantics.model.owl.Namespace;

import java.util.EnumMap;

public class ForeignKey extends Column implements CanHaveEquivalence {

    private String equivalentProperty = "";
    private Namespace equivalentPropertyNamepace = new Namespace("", "");
    private String referencedTableName;
    private String referencedColumnName;


    ForeignKey(EnumMap<Metadata, Object> foreignKeyMetadata) {
        super(new String[]{
                foreignKeyMetadata.get(Metadata.COLUMN_NAME).toString(),
                foreignKeyMetadata.get(Metadata.COLUMN_NAME).toString(),
                "",
                "",
                foreignKeyMetadata.get(Metadata.TABLE_NAME).toString().substring(0, 1).toUpperCase() + foreignKeyMetadata.get(Metadata.TABLE_NAME).toString().substring(1).toLowerCase(),
                foreignKeyMetadata.get(Metadata.REFERENCED_TABLE_NAME).toString().substring(0, 1).toUpperCase() + foreignKeyMetadata.get(Metadata.REFERENCED_TABLE_NAME).toString().substring(1).toLowerCase()});

        String range = (String) getValueAt(5);
        setValueAt(range.substring(0, 1).toUpperCase() + range.substring(1).toLowerCase(), 5);

        this.referencedTableName = foreignKeyMetadata.get(Metadata.REFERENCED_TABLE_NAME).toString();
        this.referencedColumnName = foreignKeyMetadata.get(Metadata.REFERENCED_COLUMN_NAME).toString();
    }

    public String getEquivalence() {
        return equivalentProperty;
    }

    public void setEquivalence(String equivalentProperty) {
        this.equivalentProperty = equivalentProperty;
    }

    public Namespace getEquivalenceNamespace() {
        return equivalentPropertyNamepace;
    }

    public void setEquivalenceNamespace(String fullyQualified, String prefix) {
        this.equivalentPropertyNamepace.set(fullyQualified, prefix);
    }

    @Override
    protected void setRangeName(String rangeName) {
        String rangeNameTemp = rangeName.substring(0, 1).toUpperCase() + rangeName.substring(1).toLowerCase();
        super.range.setText(rangeNameTemp);
        setValueAt(rangeNameTemp, 5);
    }

    public String getReferencedTableName() {
        return this.referencedTableName;
    }

    public String getReferencedColumnName() {
        return referencedColumnName;
    }

    public String iconPathString() {
        if (getEquivalence().equals(""))
            return "/foreign_24x24.png";
        return "/foreign_24x24_ticked.png";
    }

    @Override
    public boolean isEditable(int columnIndex) {
        return columnIndex > 0 && columnIndex < 4;
    }

}