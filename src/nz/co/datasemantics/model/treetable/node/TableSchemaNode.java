package nz.co.datasemantics.model.treetable.node;

public abstract class TableSchemaNode extends Node {

    public TableSchemaNode(Object[] objectArray) {
        super(objectArray);
        Node.setColumnCount(objectArray.length);
    }

    @Override
    public String iconPathString() {
        return ".\\resources\\database_24x24.png";
    }

    @Override
    public boolean isEditable(int columnIndex) {
        return columnIndex > 0 && columnIndex < 4;
    }
}