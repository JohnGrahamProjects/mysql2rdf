package nz.co.datasemantics.model.treetable.node;

public abstract class TableNode extends Node {

    public TableNode(Object[] objectArray) {
        super(objectArray);
    }

    @Override
    public boolean isEditable(int columnIndex) {
        return columnIndex > 0 && columnIndex < 4;
    }
}
