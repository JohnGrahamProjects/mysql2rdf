package nz.co.datasemantics.model.treetable.node;

import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;

import javax.swing.table.TableCellEditor;


public abstract class Node extends AbstractMutableTreeTableNode {

    private static int columnCount = -1;

    public String getName() {
        return getValueAt(0).toString();
    }

    public String getTerm() {
        return getValueAt(1).toString();
    }

    Node(Object[] objectArray) {
        super(objectArray);
    }

    @Override
    public Object getValueAt(int columnIndex) {
        return ((Object[]) super.getUserObject())[columnIndex];
    }

    @Override
    public void setValueAt(Object o, int columnIndex) {
        ((Object[]) super.getUserObject())[columnIndex] = o;
    }

    static void setColumnCount(int i) {
        columnCount = i;
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    public abstract String iconPathString();

    public abstract TableCellEditor getCellEditor(int columnIndex);

}
