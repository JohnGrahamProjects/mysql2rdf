package nz.co.datasemantics.model.treetable.node;

public abstract class ColumnNode extends Node {

    public ColumnNode(Object[] objects) {
        super(objects);
    }

    @Override
    public boolean isEditable(int columnIndex) {
        return columnIndex > 0;
    }
}
