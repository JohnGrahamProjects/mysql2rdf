package nz.co.datasemantics.model.treetable;

import controller.Controller;
import nz.co.datasemantics.database.MySQLConnection;
import nz.co.datasemantics.model.relational.Table;
import nz.co.datasemantics.model.treetable.node.Node;
import nz.co.datasemantics.model.treetable.node.TableSchemaNode;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.util.Arrays;

public class TreeTable {

    private final String[] columnHeadings;
    private JXTreeTable jXTreeTable;
    private TableSchemaNode tableSchemaNode;
    private Controller controller;
    public TreeTable(Controller controller, String[] columnHeadings, TableSchemaNode tableSchemaNode) {
        this.columnHeadings = columnHeadings;
        this.tableSchemaNode = tableSchemaNode;
        this.controller = controller;
    }

    public JXTreeTable getTreeTable() {

        DefaultTreeTableModel tableModel = new DefaultTreeTableModel(tableSchemaNode, Arrays.asList(columnHeadings));

        this.jXTreeTable = new JXTreeTable(tableModel) {

            @Override
            public TableCellEditor getCellEditor(int rowIndex, int columnIndex) {
                return ((Node)jXTreeTable.getPathForRow(rowIndex).getLastPathComponent()).getCellEditor(columnIndex);
            }
        };

        class AlternatingRowHighlighter extends ColorHighlighter {

            private AlternatingRowHighlighter(HighlightPredicate highlightPredicate) {
                super(highlightPredicate);
            }

            @Override
            protected Component doHighlight(Component component, ComponentAdapter componentAdapter) {
                component.setBackground(new Color(245, 245, 245));
                return component;
            }
        }

        class SelectedRowHighlighter extends ColorHighlighter {

            private SelectedRowHighlighter(HighlightPredicate highlightPredicate) {
                super(highlightPredicate);
            }

            @Override
            protected Component doHighlight(Component component, ComponentAdapter componentAdapter) {
                component.setBackground(new Color(255, 255, 225));
                return component;
            }
        }

        AlternatingRowHighlighter alternatingRowHighlighter = new AlternatingRowHighlighter(HighlightPredicate.EVEN);
        SelectedRowHighlighter selectedRowHighlighter = new SelectedRowHighlighter(HighlightPredicate.IS_SELECTED);

        class TreeIconRenderer extends DefaultTreeCellRenderer {

            @Override
            public Component getTreeCellRendererComponent(JTree jtree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                try {
                    if (row == 0 || value instanceof Table) {
                        setOpenIcon(new ImageIcon(this.getClass().getResource(((Node) value).iconPathString())));
                        setClosedIcon(new ImageIcon(this.getClass().getResource(((Node) value).iconPathString())));
                    } else {
                        setLeafIcon(new ImageIcon(this.getClass().getResource(((Node) value).iconPathString())));
                    }
                }
                catch(NullPointerException npe){}
                return super.getTreeCellRendererComponent(jtree, ((Node) value).getValueAt(0).toString(), sel, expanded, leaf, row, hasFocus);
            }
        }
        this.jXTreeTable.setHighlighters(alternatingRowHighlighter, selectedRowHighlighter);
        this.jXTreeTable.setShowGrid(
                true, true);

        this.jXTreeTable.setColumnControlVisible(
                true);

        this.jXTreeTable.setRootVisible(
                true);

        this.jXTreeTable.expandAll();
        this.jXTreeTable.setRowHeight(75);
        jXTreeTable.setTreeCellRenderer(new TreeIconRenderer());

        new PopupManager(this.controller,jXTreeTable);

        return this.jXTreeTable;
    }
}