package nz.co.datasemantics.model.treetable;

import controller.Controller;
import nz.co.datasemantics.model.owl.CanHaveEquivalence;
import nz.co.datasemantics.model.relational.Table;
import org.jdesktop.swingx.JXTreeTable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class PopupManager extends MouseAdapter implements ActionListener {

    private SetEquivalencyForm setEquivalencyForm;
    private JXTreeTable jxTreeTable;
    private PopupMenu popupMenu;
    private int currentRow = -1;
    private JMenuItem serialiseOption;
    public final Controller controller;

    PopupManager(Controller controller, JXTreeTable jxTreeTable) {
        this.initSetEquivalenceForm();
        this.initPopupMenu();
        this.controller = controller;
        this.jxTreeTable = jxTreeTable;
        this.jxTreeTable.addMouseListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        setEquivalencyForm.activate(this.jxTreeTable, this.currentRow);
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == 1) return;
        Point point = mouseEvent.getPoint();
        int currentRowTemp = this.currentRow = jxTreeTable.rowAtPoint(point);
        final int currentColumn = jxTreeTable.columnAtPoint(point);
        Rectangle rectangle = jxTreeTable.getCellRect(currentRowTemp, currentColumn, false);
        jxTreeTable.setRowSelectionInterval(currentRowTemp, currentRowTemp);
        if (popupMenu.isVisible()) {
            this.popupMenu.setVisible(false);
        } else if (jxTreeTable.getPathForRow(currentRowTemp).getLastPathComponent() instanceof CanHaveEquivalence) {
            if(jxTreeTable.getPathForRow(currentRowTemp).getLastPathComponent() instanceof Table) {
                if(serialiseOption!=null)
                    this.popupMenu.remove(serialiseOption);
                serialiseOption = new JMenuItem("Generate RDF for this table");
                serialiseOption.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        controller.serialiseRDF(((Table) jxTreeTable.getPathForRow(currentRowTemp).getLastPathComponent()).getName());
                    }
                });
                this.popupMenu.add(serialiseOption);
            } else {
                this.popupMenu.remove(serialiseOption);
            }
            this.popupMenu.show(jxTreeTable, rectangle.x, rectangle.y + rectangle.height);

        }
    }

    private void initPopupMenu() {
        popupMenu = new PopupMenu();
        JMenuItem editEquivalency = new JMenuItem("Edit OWL equivalency");
        editEquivalency.addActionListener(this);
        popupMenu.add(editEquivalency);
    }

    private void initSetEquivalenceForm() {
        setEquivalencyForm = new SetEquivalencyForm();
        setEquivalencyForm.setLocationRelativeTo(null);
        setEquivalencyForm.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    }
}