package nz.co.datasemantics.model.treetable;

import org.jdesktop.swingx.JXTreeTable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class PopupMenu extends JPopupMenu {
    private transient TableMouseListener tableMouseListener;

    public PopupMenu() {
        super();
    }

    PopupMenu(JXTreeTable jxTreeTable) {
        super();

        tableMouseListener = new TableMouseListener(jxTreeTable);
        ContextMenuListener contextMenuListener = new ContextMenuListener(jxTreeTable);

        JMenuItem editEquivalency = new JMenuItem("Edit OWL equivalency");
        add(editEquivalency);
        editEquivalency.addMouseListener(contextMenuListener);
        JMenuItem cancel = new JMenuItem("Cancel");
        add(cancel);
        jxTreeTable.addMouseListener(tableMouseListener);
    }

    class ContextMenuListener extends TableMouseListener {

        ContextMenuListener(JXTreeTable jxTreeTable) {
            super(jxTreeTable);
        }

        @Override
        public void mousePressed(MouseEvent event) {

        }
    }

    public class TableMouseListener extends MouseAdapter {

        JXTreeTable jxTreeTable;
        private int currentRow;

        TableMouseListener(JXTreeTable jxTreeTable) {
            this.jxTreeTable = jxTreeTable;
        }

        int getCurrentRow() {
            return currentRow;
        }

        @Override
        public void mousePressed(MouseEvent event) {
            Point point = event.getPoint();
            currentRow = jxTreeTable.rowAtPoint(point);
            jxTreeTable.setRowSelectionInterval(currentRow, currentRow);

        }
    }
}
