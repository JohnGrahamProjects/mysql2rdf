package nz.co.datasemantics.model.treetable;

import nz.co.datasemantics.model.owl.CanHaveEquivalence;
import nz.co.datasemantics.model.treetable.node.Node;
import org.jdesktop.swingx.JXTreeTable;

class SetEquivalencyForm extends javax.swing.JFrame {

    private javax.swing.JTextField equivalency;
    private javax.swing.JTextField namespace;
    private javax.swing.JTextField prefix;

    private javax.swing.JLabel titleLabel;

    private transient Node node = null;

    SetEquivalencyForm() {
        initComponents();
        setResizable(false);
    }

    void activate(JXTreeTable jxTreeTable, int currentRow) {
        this.setVisible(true);
        this.titleLabel.setText("Set equivalency for: " + jxTreeTable.getValueAt(currentRow, 1).toString() + " ");
        node = (Node) jxTreeTable.getPathForRow(currentRow).getLastPathComponent();

        equivalency.setText(((CanHaveEquivalence) node).getEquivalence());
        namespace.setText(((CanHaveEquivalence) node).getEquivalenceNamespace().getFullyQualified());
        prefix.setText(((CanHaveEquivalence) node).getEquivalenceNamespace().getPrefix());
    }

    private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        javax.swing.JLabel equivLabel = new javax.swing.JLabel();
        javax.swing.JLabel namespaceLabel = new javax.swing.JLabel();
        javax.swing.JLabel prefixLabel = new javax.swing.JLabel();

        equivalency = new javax.swing.JTextField();
        namespace = new javax.swing.JTextField();
        prefix = new javax.swing.JTextField();

        javax.swing.JButton save = new javax.swing.JButton();
        javax.swing.JButton cancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        titleLabel.setFont(new java.awt.Font("Tahoma", 3, 14));
        titleLabel.setText("Equivalent Class");
        titleLabel.setToolTipText("");

        equivLabel.setText("OWL Equivalency");

        namespaceLabel.setText("Namepace");

        prefixLabel.setText("Namespace Prefix");

        equivalency.setText("");
        equivalency.setToolTipText("");

        namespace.setText("");

        prefix.setText("");
        prefix.setName("prefix");

        save.setText("Save");
        save.setName("saveButton");

        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });

        cancel.setText("Cancel");
        cancel.setName("cancelButton");
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(titleLabel)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(equivLabel)
                                                        .addComponent(namespaceLabel)
                                                        .addComponent(prefixLabel))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(namespace, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                                        .addComponent(prefix)
                                                        .addComponent(equivalency)

                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(cancel)
                                                                .addGap(10, 10, 10)
                                                                .addComponent(save))
                                                )))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(titleLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(equivLabel)
                                        .addComponent(equivalency, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(namespaceLabel)
                                        .addComponent(namespace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(prefixLabel)
                                        .addComponent(prefix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(save)
                                        .addComponent(cancel))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        equivLabel.getAccessibleContext().setAccessibleName("equivalencyLabel");
        namespaceLabel.getAccessibleContext().setAccessibleName("namespaceLabel");
        prefixLabel.getAccessibleContext().setAccessibleName("prefixLabel");
        save.getAccessibleContext().setAccessibleName("saveButton");
        cancel.getAccessibleContext().setAccessibleName("cancelButton");

        pack();
    }

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.setVisible(false);
    }

    private void saveActionPerformed(java.awt.event.ActionEvent evt) {
        ((CanHaveEquivalence) node).setEquivalence(this.equivalency.getText());
        ((CanHaveEquivalence) node).setEquivalenceNamespace(this.namespace.getText(), this.prefix.getText());

        equivalency.setText("");
        namespace.setText("");
        prefix.setText("");

        this.setVisible(false);
    }
}
