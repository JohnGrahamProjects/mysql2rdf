package controller;

import com.sun.awt.AWTUtilities;
import nz.co.datasemantics.database.ConnectionParameter;
import nz.co.datasemantics.database.MySQLConnection;
import nz.co.datasemantics.model.owl.OWL;
import nz.co.datasemantics.model.rdf.RDF;
import nz.co.datasemantics.model.relational.TableSchema;
import nz.co.datasemantics.model.treetable.TreeTable;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.jdesktop.swingx.JXTreeTable;
import view.ConnectDialog;
import view.MainInterface;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;

public class Controller {

    private static TableSchema tableSchema;
    private static MySQLConnection mySQLConnection;
    private ConnectDialog connectDialogue = null;
    private MainInterface mainInterface = null;
    private JWindow processingGIF = new JWindow();

    public Controller() {


        ImageIcon icon = new ImageIcon(this.getClass().getResource("/loading.gif"));
        JLabel label = new JLabel(icon);

        processingGIF.setLayout(new BorderLayout());
        processingGIF.add(label, BorderLayout.CENTER);
        processingGIF.setLocationRelativeTo(null);
        mainInterface = new MainInterface(processingGIF, this);
        Dimension windowSize = mainInterface.getSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Point centerPoint = ge.getCenterPoint();
        int dx = centerPoint.x - 4 * windowSize.width / 12;
        int dy = centerPoint.y - 4 * windowSize.height / 12;

        processingGIF.setLocation(dx, dy);
        processingGIF.setOpacity(0.5f);
        processingGIF.setAlwaysOnTop(true);
        label.setOpaque(false);
        AWTUtilities.setWindowOpaque(processingGIF, false);
        processingGIF.pack();


        mainInterface.setVisible(true);
    }

    public void saveTabbedDocument(RSyntaxTextArea rSyntaxTextArea, File file) {
        try (FileWriter fileWriter = new FileWriter(file)) {
            BufferedWriter outFile;
            outFile = new BufferedWriter(fileWriter);
            rSyntaxTextArea.write(outFile);
            outFile.close();
        } catch (IOException ex) {
            System.out.println("Exception thrown in Controller.saveTabbedDocument Method.");
            System.out.println("EXCEPTION MESSAGE: " + ex.getMessage());
        }
    }

    public void showConnectDialog() {
        if (connectDialogue == null) {
            connectDialogue = new ConnectDialog(processingGIF, this);
        }
        connectDialogue.setVisible(true);
    }

    public void connect(Map<ConnectionParameter, String> connectionParams) {

        Controller c = this;

        class R extends Thread {

            @Override
            public void run() {
                processingGIF.setVisible(true);

                connectDialogue.getjTextArea().setText("");
                mySQLConnection = new MySQLConnection(connectionParams);
                if (mySQLConnection.connect(connectDialogue.getjTextArea())) {

                    connectDialogue.setVisible(false);

                    tableSchema = new TableSchema("university");
                    tableSchema.buildSchema(mySQLConnection);

                    TreeTable treeTable = new TreeTable(c, new String[]{"Database", "Term", "Namespace", "Namspace Prefix", "Domain", "Range"}, tableSchema);

                    tableSchema.setFullNamespace("http://www.example.com/example");
                    tableSchema.setNamespacePrefix("exa");

                    JXTreeTable jXTreeTable = treeTable.getTreeTable();
                    mainInterface.setTreeTable(jXTreeTable);
                    mainInterface.showTreeTableTab(true);
                }
                processingGIF.setVisible(false);

            }
        }
        R r = new R();
        r.start();


    }

    public void serialiseOntology(RSyntaxTextArea rSyntaxTextArea) {
        if (tableSchema == null) return;
        OWL owl = new OWL(new nz.co.datasemantics.model.owl.Namespace(tableSchema.getFullNamespace(), tableSchema.getNamespacePrefix()), tableSchema.getName());
        owl.buildGraph(tableSchema);
        nz.co.datasemantics.model.owl.Serialiser owlSerialiser = new nz.co.datasemantics.model.owl.Serialiser();

        try (OutputStream outputStream = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                String s = String.valueOf((char) b);
                rSyntaxTextArea.append(s);
            }
        }) {
            outputStream.write(owlSerialiser.serialise_ttl(owl, outputStream).getBytes(Charset.forName("UTF-8")));
        } catch (IOException e) {
            System.out.println("Exception thrown in Controller.serialiseOntology Method.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
        }
    }

    public void serialiseRDF(RSyntaxTextArea rSyntaxTextArea, String tableName) {
        if (tableSchema == null) return;
        RDF rdf = new RDF(
                new nz.co.datasemantics.model.owl.Namespace("http://www.example.com/data", "dat"));

        try (OutputStream outputStream = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                String s = String.valueOf((char) b);
                rSyntaxTextArea.append(s);
            }
        }) {
            rdf.buildAndSerialiseGraph(mySQLConnection, tableSchema, tableName, outputStream);

        } catch (IOException e) {
            System.out.println("Exception thrown in Controller.serialiseRDF Method.");
            System.out.println("EXCEPTION MESSAGE: " + e.getMessage());
        }
    }

    public void serialiseRDF(String tableName) {
        mainInterface.serializeRDF(tableName);
    }
}
