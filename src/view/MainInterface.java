package view;

import controller.Controller;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.jdesktop.swingx.JXTreeTable;
import view.custom.CJPanel;
import view.custom.ScrollFontListener;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment;
import static java.lang.Integer.parseInt;
import static java.lang.System.out;
import static java.util.regex.Pattern.compile;
import static javax.swing.BorderFactory.createEmptyBorder;
import static javax.swing.GroupLayout.Alignment.LEADING;
import static javax.swing.JFileChooser.APPROVE_OPTION;
import static org.fife.ui.rsyntaxtextarea.SyntaxConstants.SYNTAX_STYLE_XML;

public final class MainInterface extends javax.swing.JFrame {

    private final static int minWidth = 700;
    private final static int minHeight = 500;
    private final Dimension maxDimension;
    private final Dimension minDimension;
    private final Font defaultFont;
    private final Color texAreaBackgroundColor = new Color(245, 245, 245);
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenu mainMenu_file;
    private javax.swing.JMenu mainMenu_serialise;
    private javax.swing.JPanel treeTableTab;
    private javax.swing.JMenuItem subMenu_import;
    private javax.swing.JMenuItem subMenu_new;
    private javax.swing.JMenuItem subMenu_open;
    private javax.swing.JMenuItem subMenu_saveAs;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JMenuItem subMenu_serialiseOntology;
    private javax.swing.JMenuItem subMenu_serialiseRDF;

    private transient Controller controller;

    private JWindow processingGIF;

    public MainInterface(JWindow processingGIF, Controller controller) {
        this.processingGIF = processingGIF;
        this.controller = controller;
        this.defaultFont = new Font("Aller Sans", Font.PLAIN, 18);
        this.maxDimension = new Dimension(getScreenWorkingHeight(), getScreenWorkingWidth());
        this.minDimension = new Dimension(minWidth + 100, minHeight + 100);

        initComponents();

        setInterfaceColors(new Color(102, 102, 102), new Color(245, 245, 245));
        setLookAndFeel(defaultFont);
        this.setMinimumSize(minDimension);
        pack();

        showTreeTableTab(false);
        setLocationRelativeTo(null);


    }

    private static int getScreenWorkingWidth() {
        return getLocalGraphicsEnvironment().getMaximumWindowBounds().width;
    }

    private static int getScreenWorkingHeight() {
        return getLocalGraphicsEnvironment().getMaximumWindowBounds().height;
    }

    private static File getSelectedFileWithExtension(JFileChooser c) {
        File file = c.getSelectedFile();
        if (c.getFileFilter() instanceof FileNameExtensionFilter) {
            String[] exts = ((FileNameExtensionFilter) c.getFileFilter()).getExtensions();
            String nameLower = file.getName().toLowerCase();
            for (String ext : exts) {
                if (nameLower.endsWith('.' + ext.toLowerCase())) {
                    return file;
                }
            }
            file = new File(file.toString() + '.' + exts[0]);
        }
        return file;
    }

    public void setTreeTable(JXTreeTable jXTreeTable) {
        jScrollPane2.setViewportView(jXTreeTable);
        jXTreeTable.getTableHeader().setFont(defaultFont);
        mainMenu_serialise.setEnabled(true);
    }

    public void showTreeTableTab(boolean add) {
        if (!add && tabbedPane.getComponentZOrder(treeTableTab) != -1) {
            tabbedPane.remove(treeTableTab);
            mainMenu_serialise.setEnabled(false);
        } else if (add) {
            tabbedPane.addTab("schema", treeTableTab);
            int index;
            setTabTitleComponents(index = tabbedPane.getTabCount() - 1, "schema");
            tabbedPane.setSelectedIndex(index);
        }
    }

    private void setLookAndFeel(Font defaultFont) {
        UIDefaults laf = UIManager.getLookAndFeelDefaults();
        laf.put("defaultFont", defaultFont);
    }

    private void setInterfaceColors(Color backgroundColor, Color foregroundColor) {
        menuBar.setBackground(backgroundColor);
        menuBar.setForeground(foregroundColor);
        mainMenu_file.setBackground(backgroundColor);
        mainMenu_file.setForeground(foregroundColor);
        subMenu_new.setBackground(backgroundColor);
        subMenu_new.setForeground(foregroundColor);
        subMenu_open.setBackground(backgroundColor);
        subMenu_open.setForeground(foregroundColor);
        subMenu_import.setBackground(backgroundColor);
        subMenu_import.setForeground(foregroundColor);
        subMenu_saveAs.setBackground(backgroundColor);
        subMenu_saveAs.setForeground(foregroundColor);
        mainMenu_serialise.setBackground(backgroundColor);
        mainMenu_serialise.setForeground(foregroundColor);
        subMenu_serialiseOntology.setBackground(backgroundColor);
        subMenu_serialiseOntology.setForeground(foregroundColor);
        subMenu_serialiseRDF.setBackground(backgroundColor);
        subMenu_serialiseRDF.setForeground(foregroundColor);
    }

    private void initComponents() {

        tabbedPane = new javax.swing.JTabbedPane();
        treeTableTab = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        menuBar = new javax.swing.JMenuBar();
        mainMenu_file = new javax.swing.JMenu();
        subMenu_new = new javax.swing.JMenuItem();
        subMenu_open = new javax.swing.JMenuItem();
        subMenu_import = new javax.swing.JMenuItem();
        subMenu_saveAs = new javax.swing.JMenuItem();
        mainMenu_serialise = new javax.swing.JMenu();
        subMenu_serialiseOntology = new javax.swing.JMenuItem();
        subMenu_serialiseRDF = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("MySQL to RDF");

        setIconImage(new ImageIcon(this.getClass().getResource("/xml-icon.png")));

        tabbedPane.setToolTipText("");
        tabbedPane.setAutoscrolls(true);

        GroupLayout schemaTabLayout = new GroupLayout(treeTableTab);
        treeTableTab.setLayout(schemaTabLayout);
        schemaTabLayout.setHorizontalGroup(
                schemaTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane2, minWidth, minWidth, getScreenWorkingWidth())
        );
        schemaTabLayout.setVerticalGroup(
                schemaTabLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(schemaTabLayout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addComponent(jScrollPane2, minHeight, minHeight, getScreenWorkingHeight())
                                .addGap(0, 0, 0))
        );

        mainMenu_file.setText("File");
        mainMenu_file.addMenuListener(new javax.swing.event.MenuListener() {
            @Override
            public void menuCanceled(MenuEvent e) {
            }

            @Override
            public void menuDeselected(MenuEvent e) {
            }

            @Override
            public void menuSelected(MenuEvent e) {
                fileMenuSelected(e);
            }
        });

        subMenu_new.setText("New");
        subMenu_new.addActionListener(this::subMenu_newActionPerformed);
        mainMenu_file.add(subMenu_new);

        subMenu_open.setText("Open");
        subMenu_open.addActionListener(this::subMenu_openActionPerformed);
        mainMenu_file.add(subMenu_open);

        subMenu_import.setText("Import");
        subMenu_import.addActionListener(this::importSelected);
        mainMenu_file.add(subMenu_import);

        subMenu_saveAs.setText("Save As");
        subMenu_saveAs.setEnabled(false);
        subMenu_saveAs.addActionListener(this::saveSelected);
        mainMenu_file.add(subMenu_saveAs);

        menuBar.add(mainMenu_file);

        mainMenu_serialise.setText("Serialise");
        mainMenu_serialise.setEnabled(false);
        mainMenu_serialise.addMenuListener(new javax.swing.event.MenuListener() {
            @Override
            public void menuCanceled(MenuEvent e) {
            }

            @Override
            public void menuDeselected(MenuEvent e) {
            }

            @Override
            public void menuSelected(MenuEvent e) {
                mainMenu_serialiseMenuSelected(e);
            }
        });


        subMenu_serialiseOntology.setText("Ontology");
        subMenu_serialiseOntology.addActionListener(this::serialiseOntology);
        mainMenu_serialise.add(subMenu_serialiseOntology);

        subMenu_serialiseRDF.setText("RDF");
        subMenu_serialiseRDF.addActionListener(this::subMenu_serialiseRDFActionPerformed);
        mainMenu_serialise.add(subMenu_serialiseRDF);

        menuBar.add(mainMenu_serialise);

        setJMenuBar(menuBar);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(tabbedPane)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(tabbedPane, GroupLayout.Alignment.TRAILING)
        );

        tabbedPane.getAccessibleContext().setAccessibleName("tabbedPane");

        pack();
    }

    private void importSelected(ActionEvent e) {
        controller.showConnectDialog();
    }

    private void fileMenuSelected(MenuEvent e) {
        String title;
        if (tabbedPane.getTabCount() != 0
                && (title = tabbedPane.getTitleAt(tabbedPane.getSelectedIndex())) != null
                && !title.equals("schema")
                && !((CJPanel) tabbedPane.getSelectedComponent()).isSaved()) {
            subMenu_saveAs.setEnabled(true);
        } else {
            subMenu_saveAs.setEnabled(false);
        }
    }

    private void saveSelected(ActionEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Save As");
        JLabel selectedTab_label = getSelectedTabLabel();

        fileChooser.setSelectedFile(new File(selectedTab_label.getText().trim()));
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("xml files (*.xml)", "xml"));
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("xml schema files (*.xsd)", "xsd"));

        if (fileChooser.showSaveDialog(this) == APPROVE_OPTION) {
            File file;
            RSyntaxTextArea rSyntaxTextArea = getSyntaxAreaFromTab((CJPanel) tabbedPane.getSelectedComponent());
            controller.saveTabbedDocument(rSyntaxTextArea, file = getSelectedFileWithExtension(fileChooser));
            getSelectedTabLabel().setText(file.getName());
            CJPanel.setSavedStatus(true);
        }
    }

    private void serialiseOntology(ActionEvent e) {
        RSyntaxTextArea rSyntaxTextArea = addNewSyntaxTab(null);
        tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
        class R extends Thread {

            @Override
            public void run() {
                processingGIF.setVisible(true);
                controller.serialiseOntology(rSyntaxTextArea);
                R.yield();
                processingGIF.setVisible(false);
            }
        }
        R r = new R();
        r.start();
    }

    public void serializeRDF(String tableName) {
        RSyntaxTextArea rSyntaxTextArea = addNewSyntaxTab(null);
        tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
        class R extends Thread {

            @Override
            public void run() {
                processingGIF.setVisible(true);
                controller.serialiseRDF(rSyntaxTextArea, tableName);
                R.yield();
                processingGIF.setVisible(false);
            }
        }
        R r = new R();
        r.start();
    }

    private void subMenu_serialiseRDFActionPerformed(ActionEvent e) {
        this.serializeRDF(null);
    }

    private void subMenu_newActionPerformed(ActionEvent e) {
        addNewSyntaxTab(null);
        tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
    }

    private void mainMenu_serialiseMenuSelected(MenuEvent e) {
        if (tabbedPane.getTabCount() != 0 && tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()).equals("schema")) {
            subMenu_serialiseRDF.setEnabled(true);
            subMenu_serialiseRDF.setEnabled(true);
        }
    }

    private void subMenu_openActionPerformed(ActionEvent e) {
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("xml files (*.xml)", "xml"));
        jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("xml schema files (*.xsd)", "xsd"));

        int result = jFileChooser.showOpenDialog(this);

        if (result == APPROVE_OPTION) {
            File file = jFileChooser.getSelectedFile();
            try (FileReader reader = new FileReader(file)) {
                int i = 1;
                RSyntaxTextArea rSyntaxTextArea = addNewSyntaxTab(file.getName());
                reader.close();
                while (i != -1) {
                    i = reader.read();
                    char ch = (char) i;
                    rSyntaxTextArea.append(Character.toString(ch));
                }
            } catch (IOException ioe) {
                out.println(ioe.getMessage());
            }
        }
    }

    private RSyntaxTextArea getSyntaxAreaFromTab(CJPanel tab) {
        JPanel tab_innerPanel = (JPanel) tab.getComponents()[0];
        JScrollPane tab_scrollPane = (JScrollPane) tab_innerPanel.getComponents()[0];
        JViewport jViewport = (JViewport) tab_scrollPane.getComponents()[0];
        return (RSyntaxTextArea) jViewport.getComponents()[0];
    }

    private JLabel getSelectedTabLabel() {
        return (JLabel) ((JPanel) tabbedPane.getTabComponentAt(tabbedPane.getSelectedIndex())).getComponents()[0];
    }

    private void setIconImage(ImageIcon imageIcon) {
        setIconImage(imageIcon.getImage());
    }

    private RSyntaxTextArea addNewSyntaxTab(String tabName) {
        String TabName_string = tabName == null ? createUniqueTabName() : tabName;

        CJPanel tab = new CJPanel();
        CJPanel.setSavedStatus(false);

        RSyntaxTextArea tab_textArea = new RSyntaxTextArea();
        tab_textArea.setBackground(texAreaBackgroundColor);
        tab_textArea.setSyntaxEditingStyle(SYNTAX_STYLE_XML);
        tab_textArea.setCodeFoldingEnabled(true);
        tab_textArea.setFont(defaultFont);
        tab_textArea.setAutoscrolls(true);
        tab_textArea.addMouseWheelListener(new ScrollFontListener(tab_textArea));

        JScrollPane innerScrollPane = new JScrollPane();
        innerScrollPane.setViewportView(tab_textArea);

        JPanel tab_innerPanel = new javax.swing.JPanel();

        GroupLayout innerJPanelLayout = new GroupLayout(tab_innerPanel);
        tab_innerPanel.setLayout(innerJPanelLayout);
        innerJPanelLayout.setHorizontalGroup(innerJPanelLayout.createParallelGroup(LEADING)
                .addComponent(innerScrollPane, minWidth, tabbedPane.getWidth(), getScreenWorkingWidth())
        );
        innerJPanelLayout.setVerticalGroup(innerJPanelLayout.createParallelGroup(LEADING)
                .addComponent(innerScrollPane, minHeight, tabbedPane.getHeight(), getScreenWorkingHeight())
        );

        GroupLayout outerJPanelLayout = new GroupLayout(tab);
        tab.setLayout(outerJPanelLayout);
        outerJPanelLayout.setHorizontalGroup(outerJPanelLayout.createParallelGroup(LEADING)
                .addComponent(tab_innerPanel, minWidth, tabbedPane.getWidth(), getScreenWorkingWidth())
        );
        outerJPanelLayout.setVerticalGroup(outerJPanelLayout.createParallelGroup(LEADING)
                .addGroup(outerJPanelLayout.createSequentialGroup()
                        .addComponent(tab_innerPanel, minHeight, tabbedPane.getHeight(), getScreenWorkingHeight())
                        .addGap(0, 0, 0))
        );

        tabbedPane.addTab(TabName_string, tab);
        setTabTitleComponents(tabbedPane.getTabCount() - 1, TabName_string);
        tabbedPane.getAccessibleContext().setAccessibleName(TabName_string);
        tabbedPane.setPreferredSize(new Dimension(tabbedPane.getWidth(), tabbedPane.getHeight()));

        tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
        tab_textArea.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                CJPanel.setSavedStatus(false);
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        pack();
        return tab_textArea;
    }

    private void setTabTitleComponents(int tabPanel_index, String heading) {
        JPanel tab = new JPanel(new GridBagLayout());

        tab.setOpaque(false);
        JLabel tabLabel = new JLabel(heading);

        JButton closeButton = new JButton("x");
        closeButton.setBorder(createEmptyBorder(-1, 5, 1, 5));
        closeButton.setForeground(Color.RED);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 0;

        tab.add(tabLabel, constraints);

        constraints.gridx++;
        constraints.weightx = 0;
        tab.add(new JLabel("  "), constraints);
        constraints.gridx++;
        tab.add(closeButton, constraints);

        JFrame jFrame = this;
        closeButton.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                int index = tabbedPane.indexOfTabComponent(closeButton.getParent());

                if (tabbedPane.getTitleAt(index).equals("schema")) {
                    showTreeTableTab(false);
                } else if (((CJPanel) tabbedPane.getComponentAt(index)).isSaved()) {
                    tabbedPane.remove(index);
                } else {
                    Object[] options
                            = {
                            "Yes",
                            "No",
                            "Cancel"
                    };
                    int userChoice = JOptionPane.showOptionDialog(jFrame,
                            "Save '" + tabbedPane.getTitleAt(index) + "' ?",
                            "",
                            JOptionPane.YES_NO_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            options,
                            options[2]);

                    if (userChoice == 0) {
                        saveSelected(null);
                    } else if (userChoice == 1) {
                        tabbedPane.remove(index);
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        });

        tabbedPane.setTabComponentAt(tabPanel_index, tab);
    }

    private String createUniqueTabName() {
        int freeNumber;

        ArrayList<Integer> takenNumbers = new ArrayList<>();
        for (int i = 0; i < tabbedPane.getTabCount(); i++) {
            String text = ((JLabel) ((JPanel) tabbedPane.getTabComponentAt(i)).getComponents()[0]).getText();
            Pattern pattern = compile("new file.*[0-9]");
            Matcher m = pattern.matcher(text);
            if (m.find()) {
                pattern = compile("[0-9]+");
                m = pattern.matcher(text);
                if (m.find()) {
                    takenNumbers.add(parseInt(m.group()));
                }
            }
        }
        freeNumber = 0;
        while (takenNumbers.contains(freeNumber)) {
            freeNumber++;
        }
        return "new file " + freeNumber;
    }
}
