package view.custom;

import javax.swing.*;

public class CJPanel extends JPanel {

    private static boolean savedStatus = false;

    public CJPanel() {
        super();
    }

    public static void setSavedStatus(boolean savedStatus) {
        CJPanel.savedStatus = savedStatus;
    }

    public boolean isSaved() {
        return savedStatus;
    }
}
